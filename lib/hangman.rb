class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def play
    system "clear"
    welcome
    setup
    take_turn until won?
    conclude
  end

  def setup
    length = @referee.pick_secret_word
    @guesser.register_secret_length(length)
    @board = Array.new(length)
  end

  def take_turn
    guess = @guesser.guess(@board)
    indices = @referee.check_guess(guess)
    update_board(indices, guess)
    @guesser.handle_response(guess, indices)
  end

  private

  def update_board(indices, guess)
    indices.each { |idx| @board[idx] = guess}
  end

  def won?
    @board.none?(&:nil?)
  end

  def welcome
    puts "Welcome to Hangman!"
  end

  def conclude
    puts "Word has been guessed!"
  end
end

class HumanPlayer

  def initialize
    @guessed_letters = []
  end

  def pick_secret_word
    puts "Think of a word and put in how many letters."
    gets.chomp.to_i
  end

  def check_guess(letter)
    puts "What letter positions word correspond to the guess?"
    puts "ex. 1 2 5"
    indices = gets.chomp
    indices.split.map { |el| el.to_i - 1}
  end

  def register_secret_length(length)
    puts "Computer has chosen a word."
    puts "Secret word has #{length} letters."
  end

  def guess(board)
    print_board(board)
    puts "Letters guessed: #{@guessed_letters.join}"
    print "> "
    guess = gets.chomp
    if @guessed_letters.include?(guess)
      puts "You have already guessed this letter!"
      puts "Guess another letter."
      guess(board)
    end
    @guessed_letters << guess
    guess
  end

  def handle_response(guess, board)
  end

  private

  def print_board(board)
    string = board.map do |el|
      el.nil? ? "_" : el
    end.join

    puts "Secret word: #{string}"
  end
end

class ComputerPlayer

  def self.dictionary
    File.readlines("dictionary.txt").map { |line| line.chomp}
  end

  def initialize(dictionary)
    @dictionary = dictionary
    @secret_length
    @guessed_letters = []
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.each_char.with_index do |ch, idx|
      indices << idx if ch == letter
    end
    indices
  end

  def register_secret_length(length)
    @secret_length = length
    @dictionary.select! { |word| word.length == @secret_length}
  end

  def guess(board)
    print_board(board)
    hsh = Hash.new(0)
    check_word_bank
    letters = candidate_words.join
    unless board.compact.empty?
      deleted_guessed_letters = letters.delete(board.compact.join)
      deleted_guessed_letters = deleted_guessed_letters.delete(@guessed_letters.join)
      deleted_guessed_letters.each_char { |ch| hsh[ch] += 1}
    else
      letters.each_char { |ch| hsh[ch] += 1}
    end
    guess = hsh.sort_by { |ch, count| count}.last.first
    puts "Computer's guess: #{guess}"
    @guessed_letters << guess
    guess
  end

  def candidate_words
    @dictionary.sort
  end

  def handle_response(guess, indices)
    if indices.empty?
      @dictionary.reject! { |word| word.include?(guess)}
    else

      @dictionary.select! do |word|
        indices.all? { |idx| word[idx] == guess && word.count(guess) == indices.length}
      end
    end
  end

  private

  def print_board(board)
    string = board.map do |el|
      el.nil? ? "_" : el
    end.join

    puts "Secret word: #{string}"
  end

  def check_word_bank
    if candidate_words.empty?
      puts "There is no such word in my dictionary."
      system "exit"
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  dictionary = ComputerPlayer.dictionary
  players = {
    referee: HumanPlayer.new,
    guesser: ComputerPlayer.new(dictionary)
  }
  Hangman.new(players).play
end
